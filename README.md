Para poder iniciar este proyecto es necesario el archivo .env con las variables de entorno para realizar la conexión a nuestra base de datos

Por ejemplo:

DB_USERNAME=postgres
DB_PASSWORD=postgres
DB_HOST=127.0.0.1
DB_DATABASE=mibasedatos

Para instalar las dependencias puedes usar yarn o npm install

https://github.com/sequelize/cli

Para realizar las migraciones puedes usar el comando npx sequelize-cli db:migrate
Para deshacer las migraciones npx sequelize-cli db:migrate:undo:all
Para cargar las semillas npx sequelize-cli db:seed:all
'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Sessions extends Model {
    static associate(models) {
      // define association here
    }
  };
  Sessions.init({
    data: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Sessions',
    tableName: 'sessions',
    underscored: true
  });
  return Sessions;
};
'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Products extends Model {
    static associate(models) {
      // define association here
    }
  };
  Products.init({
    sku: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Products',
    tableName: 'products',
    underscored: true
  });
  return Products;
};
'use strict';

let sessionsArray = [
  {
    id: "u8sua9s8a9s",
    data: "w9oiqw1002",
    created_at: new Date(),
    updated_at: new Date()
  }
]

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let sessions = await queryInterface.bulkInsert('sessions', sessionsArray, {returning: true});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('sessions', null, {});
  }
};
